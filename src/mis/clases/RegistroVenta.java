/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author 52667
 */
public class RegistroVenta {
    private float codigoVenta;
    private int cantidad;
    private int tipo;
    private float precio;

    public RegistroVenta() {
        
    this.codigoVenta = 0.0f;    
    this.cantidad = 0;    
    this.tipo=0;
    this.precio =0.0f;     
        
    }

    public RegistroVenta(float codigoVenta, int cantidad, int tipo, float precio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }

    public float getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(float codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    
    //Calcular impuesto 
    public float impuesto() {
        float impuesto = 0.0f;
        impuesto = this.precio * 0.16f;
        return impuesto;
    }
    //calcular el total a pagar
    public float calcularTotalPagar(){
        float totalPagar = 0.0f;
        totalPagar = this.precio + this.impuesto();
        return totalPagar;
        
    }
    public void RegistroVenta(){
        System.out.println("Codigo de la venta :" + this.codigoVenta);
        System.out.println("Cantidad :" + this.cantidad);
        System.out.println("Tipo :" + this.tipo);
        System.out.println("Precio del producto :" + this.precio);
        
 
    }
    
    
    
}








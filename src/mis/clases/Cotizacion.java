/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author 52667
 */
public class Cotizacion {
    private int NumCotizacion;
    private String DescripcionAuto;
    private float precio;
    private float plazo;
    //metodos
    //metodos constructores
    public Cotizacion(){
        this.NumCotizacion=0;
        this.DescripcionAuto= "";
        this.precio=0.0f;
        this.plazo=0.0f;
}
    public Cotizacion(int NumCotizacion, String DescripcionAuto, 
            float precio, float plazo) {
        this.NumCotizacion = NumCotizacion;
        this.DescripcionAuto = DescripcionAuto;
        this.precio = precio;
        this.plazo = plazo;
    }
    public Cotizacion(Cotizacion otro) {
        this.NumCotizacion = otro.NumCotizacion;
        this.DescripcionAuto = otro.DescripcionAuto;
        this.precio = otro.precio;
        this.plazo = otro.plazo;
    }

    public int getNumCotizacion() {
        return NumCotizacion;
    }

    public void setNumCotizacion(int NumCotizacion) {
        this.NumCotizacion = NumCotizacion;
    }

    public String getDescripcionAuto() {
        return DescripcionAuto;
    }

    public void setDescripcionAuto(String DescripcionAuto) {
        this.DescripcionAuto = DescripcionAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPlazo() {
        return plazo;
    }

    public void setPlazo(float plazo) {
        this.plazo = plazo;
    }
    
    
    
    
    public float calcularPagoInicial(){
        // El pago inicial es el 22% del total a financiar
    return this.calcularTotalFinanziar() * 0.2f;     
    }
    
    public float calcularTotalFinanziar(){
            return this.precio * this.plazo;   
    }
    public float pagoMensual(){
        float totalFinanciar = this.calcularTotalFinanziar();
        float saldoDeudor = totalFinanciar - this.calcularPagoInicial();
        return (totalFinanciar * 0.1f) + (saldoDeudor * 0.05f);
    }
    public void imprimirCotizacion(){
        System.out.println("cotización: " + this.NumCotizacion);
        System.out.println("descripcion del auto" + this.DescripcionAuto);
        System.out.println("precio del auto" + this.precio);
        System.out.println("plazo a pagar" + this.plazo);
        System.out.println("pago inicial" + this.calcularPagoInicial());
        System.out.println("total a finanziar" + this.calcularTotalFinanziar());
        System.out.println("pagos mensuales" + this.pagoMensual());
        
    }

    
}
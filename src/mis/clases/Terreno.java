/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

import java.util.Scanner;

/**
 *
 * @author 52667
 */
public class Terreno {
    private float ancho;
    private float largo;
    
    // mettodos
    
    //metodo constructor
    
    public Terreno(){
        // constructor por omision
    this.ancho=0.0f;
    this.largo=0.0f;        
    }
    
    public Terreno(float ancho, float largo){
        // constructor por argumentos
        this.ancho=ancho;
        this.largo=largo;
            
    }
    
    public Terreno(Terreno x){
        // constructor por copia
        this.ancho= x.ancho;
        this.largo= x.largo;
        
    }
    
    // metodos set y get 

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    // metodos de comportamiento
    
    public float calcularPerimetro(){
        float perimetro=0.0f;
        perimetro = this.ancho *  2 + this.largo*2;
        return perimetro;
    }
    
    public float calcularArea(){
        float area=0.0f;
        area=this.ancho * this.largo;
        return area;
        
    }
    
    public void imprimirTerreno(){
        System.out.println("Ancho =" + this.ancho);
        System.out.println("Largo =" + this.largo);
        System.out.println("El perimetro es:" + this.calcularPerimetro());
        System.out.println("El area es:" + this.calcularArea());
        
    }
    
    public static void main (String[] args){
        //declaracion de variables
        
        Terreno terreno = new Terreno();
        Scanner sc = new Scanner(System.in);
        int opc = 0;
        float perimetro = 0.0f, area = 0.0f, ancho = 0.0f, largo = 0.0f;
        do{
            
            // pintar el numero 
            System.out.println("1. iniciar el objeto");
            System.out.println("2 cambiar ancho");
            System.out.println("3 cambiar largo");
            System.out.println("4 mostar informacion");
            System.out.println("5 salir");
            System.out.println("dame la opcion");
            opc = sc.nextInt();
            
            switch(opc){
                case 1:
                     System.out.println("dame lo ancho del terreno");
                      ancho = sc.nextFloat();
                      System.out.println("dame lo largo del terreno");
                      largo = sc.nextFloat();
                      terreno.setAncho(ancho);
                      terreno.setLargo(largo);
                      break;
                case 2:
                     System.out.println("dame lo ancho del terreno");
                      ancho = sc.nextFloat();
                      terreno.setAncho(ancho);
                      break;
                case 3:    
                     System.out.println("dame lo ancho del terreno");
                      largo = sc.nextFloat();
                      terreno.setAncho(ancho);
                      break;
                case 4:
                     terreno.imprimirTerreno();
                      break;
                case 5:
                    System.out.println("hasta la vista baby..........!!!22");
                      break;
                default:
                    System.out.println("no es una opcion valida...!!usa 1-5");
                    
            }
        }while(opc!=5);
    }
}

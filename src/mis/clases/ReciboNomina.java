/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author 52667
 */
public class ReciboNomina {
    int numRecibo;
    String nombre;
    int puesto; 
    int diasTrabajados;
    float nivel;
    
    public ReciboNomina(){
        this.numRecibo = 0; 
        this.nombre = "";
        this.puesto = 0; 
        this.diasTrabajados = 0;
        this.nivel = 0;    
    }

    public ReciboNomina(int numRecibo, String nombre, int puesto, int diasTrabajados, float nivel) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.diasTrabajados = diasTrabajados;
        this.nivel = nivel;
    }

    ReciboNomina(dlgMenuReciboNomina aThis, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPuesto() { 
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public float getNivel() {
        return nivel;
    }

    public void setNivel(float nivel) {
        this.nivel = nivel;
    }
    
    public float pagoPuesto(int puesto){ 
        float pago = 0;
       switch (puesto){
           case 1:
               pago = 100 * this.diasTrabajados;
               break;
           case 2:
               pago = 200 * this.diasTrabajados;
               break;
           case 3:
               pago = 300 * this.diasTrabajados;
               break;
               
       }
       return pago;
    }
    
    public float calculoImpuesto(int nivel){
        float impuesto = 0;
        switch (nivel){
            case 1:
                impuesto = 0.05f * pagoPuesto(this.puesto);
                break;
            case 2:
                impuesto = 0.03f * pagoPuesto(this.puesto);
                break;
                
        }
        return impuesto;
    }
    
    public float pagototal(float pago, float impuesto){
        return pago - impuesto; 
    }
    
    public void imprimirReciboDocentes(){
        System.out.println("Numero Del Recibo :" + this.numRecibo);
        System.out.println("Nombre :" + this.nombre);
        System.out.println("Puesto :" + this.puesto);
        System.out.println("Nivel :" + this.nivel);
        System.out.println("Dias Trabajados: " + this.diasTrabajados); 
        System.out.println("Calculo de pago :" + this.pagoPuesto(this.puesto));
        System.out.println("Calculando impuesto :" + this.calculoImpuesto((int) this.nivel));
        System.out.println("Pago Total :" + this.pagototal(this.pagoPuesto(this.puesto), this.calculoImpuesto((int) this.nivel)));
    }

    void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    
}


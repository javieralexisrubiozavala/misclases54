/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author 52667
 */
public class Factur4 {
    private int numFactura;
    private String rfc;
    private String nomCliente;
    private String domFiscal;
    private String Descripcion;
    private String fechaVentas;
    private float totalVentas;
    
    //Metodos
    //Metodos Constructores
    
    public Factur4() {
        this.numFactura =0;
        this.rfc = "";
        this.nomCliente = "";
        this.domFiscal ="";
        this.Descripcion = "";
        this.fechaVentas = "";
        this.totalVentas = 0.0f;
    
     }   
    
    
    
    public Factur4(int numFacturas, String rfc, 
    String nomCliente, String domFiscal, String Descripcion, String fechaVentas, float totalVentas) {
        this.numFactura = numFacturas;
        this.rfc = rfc;
        this.nomCliente = nomCliente;
        this.domFiscal = domFiscal;
        this.Descripcion = Descripcion;
        this.fechaVentas = fechaVentas;
        this.totalVentas = totalVentas;
    }
    public Factur4(Factur4 otro){
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nomCliente = otro.nomCliente;
        this.domFiscal = otro.domFiscal;
        this.Descripcion = otro.Descripcion;
        this.fechaVentas = otro.fechaVentas;
        this.totalVentas = otro.totalVentas;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    public String getDomFiscal() {
        return domFiscal;
    }

    public void setDomFiscal(String domFiscal) {
        this.domFiscal = domFiscal;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getFechaVentas() {
        return fechaVentas;
    }

    public void setFechaVentas(String fechaVentas) {
        this.fechaVentas = fechaVentas;
    }

    public float getTotalVentas() {
        return totalVentas;
    }

    public void setTotalVentas(float totalVentas) {
        this.totalVentas = totalVentas;
    }
    
    
    
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto= this.totalVentas * .16f;
        return impuesto; 
    }
    
    public float calcularTotalPagar(){
        float total= 0.0f;
        total= this.totalVentas + this.calcularImpuesto();
        return total;
    }
     public void imprimirFacturas(){
        System.out.println("numero de factura :" + this.numFactura);
        System.out.println("RFC :" + this.rfc);
        System.out.println("nombre del cliente :" + this.nomCliente);
        System.out.println("domicilio fical :" + this.domFiscal);
        System.out.println("descripcion :" + this.Descripcion);
        System.out.println("fecha de venta :" + this.fechaVentas);
        System.out.println("total de venta" + this.totalVentas);
        System.out.println("impuesto" + this.calcularImpuesto());
        System.out.println("total pagar" + this.calcularTotalPagar());
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author 52667
 */

public class EmpleadosDocentes {
    int numDocente; 
    String nombre;
    String domicilio;
    int nivel;
    float pagoHoraImpartida;
    int horasImpartidas;
    private int numHijos;

    public EmpleadosDocentes() {
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0; 
        this.pagoHoraImpartida = 0;
        this.horasImpartidas = 0;
    }

    public EmpleadosDocentes(int numDocente, String nombre, String domicilio, int nivel, float pagoHoraImpartida, int horasImpartidas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoHoraImpartida = pagoHoraImpartida;
        this.horasImpartidas = horasImpartidas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoHoraImpartida() {
        return pagoHoraImpartida;
    }

    public void setPagoHoraImpartida(float pagoHoraImpartida) {
        this.pagoHoraImpartida = pagoHoraImpartida;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public void setNumHijos(int numHijos) {
        this.numHijos = numHijos;
    }
    
    public float pago(){
        return this.horasImpartidas * this.pagoHoraImpartida;
    }
    
    public float impuesto(){

        return this.pagoHoraImpartida;
    }
    
    public float calcularBono(int numHijos) {
        float bono = 0;

        if (numHijos >= 1 && numHijos <= 2) {
            bono = this.pago() * 0.05f; 
        } else if (numHijos >= 3 && numHijos <= 5) {
            bono = this.pago() * 0.10f; 
        } else if (numHijos > 5) {
            bono = this.pago() * 0.20f; 
        }

        return bono;
    }        
    public float pago(int pago){
    float bono = calcularBono(0); 
    float pagoTotal = this.horasImpartidas * this.pagoHoraImpartida;
    return pagoTotal + bono;
    }
    public float pagoTotal() {
     
    float bonoPaternidad = this.calcularBono(this.numHijos); 
    float pagoHoras = this.pago(); 
    return bonoPaternidad + pagoHoras; 
    }

    public void imprimirEmpleadosDocentes(){
        System.out.println("Numero Del Docente :" + this.numDocente);
        System.out.println("Nombre :" + this.nombre);
        System.out.println("Domicilio :" + this.domicilio);
        System.out.println("Nivel :" + this.nivel);
        System.out.println("Bono por paternidad: " + this.calcularBono(this.numHijos)); 
        System.out.println("Pago Por Horas Impartidas :" + this.pago());
        System.out.println("Horas Impartidas :" + this.horasImpartidas);
        System.out.println("Pago Total :" + this.pagoTotal());
    }

    
}
 
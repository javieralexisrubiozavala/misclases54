/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

import javax.swing.JOptionPane;

/**
 *
 * @author 52667
 */
public class JintReciboNomina extends javax.swing.JFrame {
        ;
    /**
     * Creates new form JintReciboNomina
     */
    public JintReciboNomina() {
        initComponents();
        setSize(450,400);
        
        this.setLocationRelativeTo(null);
        this.deshabilitar();
    }

    JintReciboNomina(dlgMenuReciboNomina aThis, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void habilitar(){
        this.txtDiasTrabajados.setEnabled(!false);
        this.txtImpuesto.setEnabled(!false);
        this.txtImpuesto5.setEnabled(!false);
        this.txtNombre.setEnabled(!false);
        this.txtNumeroRecibo.setEnabled(!false );
        this.txtPago.setEnabled(!false );
       
        
        this.btmNuevo.setEnabled(!false);
        this.btmGuardar.setEnabled(!false);
        this.btmMostrar.setEnabled(!false);
        this.btmLimpiar.setEnabled(!false);
            
    }
    public void deshabilitar(){
        this.txtDiasTrabajados.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtImpuesto5.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtNumeroRecibo.setEnabled(false);
        this.txtPago.setEnabled(false);

        
    }
    public void limpiar(){
        this.txtDiasTrabajados.setText("");
        this.txtImpuesto.setText("");
        this.txtImpuesto5.setText("");
        this.txtNombre.setText("");
        this.txtNumeroRecibo.setText("");
        this.txtPago.setText("");
   
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtPago = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtImpuesto5 = new javax.swing.JTextField();
        txtNumeroRecibo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        cmbPuesto = new javax.swing.JComboBox<>();
        cmbNivel = new javax.swing.JComboBox<>();
        txtDiasTrabajados = new javax.swing.JTextField();
        btmNuevo = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmMostrar = new javax.swing.JButton();
        btmLimpiar = new javax.swing.JButton();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jLabel9.setText("jLabel9");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 300));
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Numero de Recibo");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(20, 30, 101, 15);

        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 60, 50, 14);

        jLabel3.setText("Puesto");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 90, 50, 20);

        jLabel4.setText("Nivel");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(20, 120, 40, 20);

        jLabel5.setText("Dias Trabajados");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(20, 150, 100, 20);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setToolTipText("");
        jPanel1.setName(""); // NOI18N
        jPanel1.setNextFocusableComponent(this);
        jPanel1.setOpaque(false);
        jPanel1.setLayout(null);

        jLabel6.setText("Calculo pago");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 30, 80, 14);

        jLabel7.setText("Calculo Impuesto");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 60, 120, 14);

        jLabel8.setText("Calculo Imouesto 5%");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(10, 90, 120, 14);

        txtPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoActionPerformed(evt);
            }
        });
        jPanel1.add(txtPago);
        txtPago.setBounds(130, 20, 130, 25);

        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        jPanel1.add(txtImpuesto);
        txtImpuesto.setBounds(130, 50, 130, 25);
        jPanel1.add(txtImpuesto5);
        txtImpuesto5.setBounds(130, 80, 130, 25);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 190, 370, 130);
        jPanel1.getAccessibleContext().setAccessibleName("");

        txtNumeroRecibo.setMinimumSize(new java.awt.Dimension(10, 30));
        txtNumeroRecibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroReciboActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumeroRecibo);
        txtNumeroRecibo.setBounds(140, 30, 60, 25);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(140, 60, 170, 25);

        cmbPuesto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Auxiliar", "2 Albañil", "3 Ing. Obra", " " }));
        getContentPane().add(cmbPuesto);
        cmbPuesto.setBounds(140, 90, 81, 20);

        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Base", "2 Eventual" }));
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(140, 120, 76, 20);
        getContentPane().add(txtDiasTrabajados);
        txtDiasTrabajados.setBounds(140, 150, 140, 25);

        btmNuevo.setText("Nuevo");
        btmNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btmNuevo);
        btmNuevo.setBounds(340, 20, 70, 23);

        btmGuardar.setText("Guardar");
        btmGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(340, 60, 71, 23);

        btmMostrar.setText("Mostrar");
        btmMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmMostrar);
        btmMostrar.setBounds(340, 100, 70, 23);

        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(20, 330, 65, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumeroReciboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroReciboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroReciboActionPerformed

    private void btmNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmNuevoActionPerformed
        // TODO add your handling code here:
        reci = new ReciboNomina();
        this.habilitar();
        
    }//GEN-LAST:event_btmNuevoActionPerformed

    private void btmGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito = false;
    if (txtNumeroRecibo.getText().equals("")) exito = true;
    if (txtNombre.getText().equals("")) exito = true;
    if (cmbPuesto.getSelectedIndex()== -1) exito = true;
    if (cmbNivel.getSelectedIndex() == -1) exito = true;
    if (txtDiasTrabajados.getText().equals("")) exito = true;

    if (exito == true) {
        JOptionPane.showMessageDialog(this, "Informacion faltante, favor de añadir", "Advertencia", JOptionPane.WARNING_MESSAGE);
    } else {
        reci = new ReciboNomina();
        reci.setNumRecibo(Integer.parseInt(txtNumeroRecibo.getText()));
        reci.setNombre(txtNombre.getText());
        reci.setPuesto(cmbPuesto.getSelectedIndex() + 1);
        reci.setNivel(cmbNivel.getSelectedIndex() + 1); 
        reci.setDiasTrabajados(Integer.parseInt(txtDiasTrabajados.getText()));
        

        JOptionPane.showMessageDialog(this, "Se guardó con éxito");
        btmMostrar.setEnabled(true);
    }
    }//GEN-LAST:event_btmGuardarActionPerformed

    private void btmMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmMostrarActionPerformed
        // TODO add your handling code here:
        float pago = reci.pagoPuesto((int) reci.getPuesto()); 
        float impuesto = reci.calculoImpuesto((int) reci.getNivel()); 
        float total = reci.pagototal(pago, impuesto); 

        
        this.txtPago.setText(String.valueOf(pago));
        this.txtImpuesto.setText(String.valueOf(impuesto));
        this.txtImpuesto5.setText(String.valueOf(total));

    }//GEN-LAST:event_btmMostrarActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.txtNumeroRecibo.setText("");
        this.txtNombre.setText("");
        this.cmbPuesto.setSelectedIndex(0);
        this.cmbNivel.setSelectedIndex(0);
        this.txtDiasTrabajados.setText("");
        this.txtImpuesto.setText("");
        this.txtImpuesto5.setText("");
        this.txtPago.setText("");
        
        
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void txtPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JintReciboNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JintReciboNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JintReciboNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JintReciboNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JintReciboNomina().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmGuardar;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JButton btmMostrar;
    private javax.swing.JButton btmNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JComboBox<String> cmbPuesto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList<String> jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField txtDiasTrabajados;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtImpuesto5;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumeroRecibo;
    private javax.swing.JTextField txtPago;
    // End of variables declaration//GEN-END:variables
    private ReciboNomina reci = new ReciboNomina();

}
